import { IDrawable } from '../canvas-renderer/drawable';
import { Sky } from './sky';

export class BackgroundComponent implements IDrawable {
  private sky: Sky;
  private canvas: HTMLCanvasElement;

  constructor(canvas: HTMLCanvasElement) {
    this.sky = new Sky();
    this.canvas = canvas;
  }

  public draw(dt: number) {
    const context2d = this.canvas.getContext('2d');

    const image = new Image();
    image.src = 'assets/images/background/BG_morning_evening.png';

    context2d.drawImage(image, 0, 0, 1, 480, 0, 0, this.canvas.width, this.canvas.height);
  }
}
