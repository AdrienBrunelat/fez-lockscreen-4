import { BackgroundComponent } from './background/background-component';
import { CanvasRenderer } from './canvas-renderer/canvas-renderer';
import { GomezComponent } from './gomez/gomez-component';
// Stylesheet
import './scss/styles.scss';

const main = () => {
  const gomez = new GomezComponent(document.getElementById('gomez') as HTMLCanvasElement);
  const background = new BackgroundComponent(document.getElementById('background') as HTMLCanvasElement);

  const renderer = new CanvasRenderer();
  renderer.addDrawable(background);
  renderer.addDrawable(gomez);
  renderer.render();
};

main();
