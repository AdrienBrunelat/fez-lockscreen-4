export interface IDrawable {
  draw(dt: number): void;
}
