import { IDrawable } from './drawable';

export class CanvasRenderer {
  private drawable: IDrawable[] = [];
  private time: number;

  public addDrawable(drawable: IDrawable): void {
    this.drawable.push(drawable);
  }

  public render(): void {
    window.requestAnimationFrame((now) => {
      this.frame(now);
    });
  }

  private frame(then): void {
    window.requestAnimationFrame((now) => {
      this.frame(now);
    });

    this.update(then);
  }

  private update(now: number): void {
    const dt = Math.min(1000, now - (this.time || now)) || 0;
    this.time = now;

    this.drawable.forEach((component) => component.draw(dt));
  }
}
