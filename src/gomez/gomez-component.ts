import { IDrawable } from '../canvas-renderer/drawable';
import { configuration } from '../configuration';
import { Gomez } from './gomez';

export class GomezComponent implements IDrawable {
  private gomez: Gomez;
  private canvas: HTMLCanvasElement;

  constructor(canvas: HTMLCanvasElement) {
    this.gomez = new Gomez();
    this.canvas = canvas;
  }

  public draw(dt: number) {
    const context2d = this.canvas.getContext('2d');

    context2d.clearRect(0, 0, this.canvas.width, this.canvas.height);

    const image = new Image();
    image.src = 'assets/images/gomez/gomez_up.png';

    context2d.imageSmoothingEnabled = false;
    context2d.drawImage(image, 0, 0, 13, 19, 100, 100, 13 * configuration.pixelScale, 19 * configuration.pixelScale);
  }
}
